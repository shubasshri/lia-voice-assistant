import React, { Component } from "react";
import Amplify, { Interactions } from "aws-amplify";
import { ChatBot, AmplifyTheme } from "aws-amplify-react";
import awsconfig from "../../aws-exports";

import liaStatic from "../../assets/lia-listen.gif";
import liaListen from "../../assets/lia-listen.gif";
import liaSpeaking from "../../assets/lia-speaking.gif";


Amplify.configure(awsconfig);

const myTheme = {
  ...AmplifyTheme,
  sectionHeader: {
    ...AmplifyTheme.sectionHeader,
    backgroundColor: '#ff6600'
  }
};

const customVoiceConfig = {
    silenceDetectionConfig: {
        time: 2500,
        amplitude: 0.2
    }   
};

export class HomePage extends Component {

    constructor(props) {
        super(props);
    }
    handleComplete(err, confirmation) {
        if (err) {
            return "Sorry that I couldn't help you!";
        }
        return '';
  }

  render() {
    return (
        <div className="home-page">
            <div id="home-page-header" className="home-page-header">
                <p className="nav-title">Lia - Logitech Intelligent Assistant</p>
            </div>
            <div className="content">
                <img src={liaStatic} className="brand-logo" alt="logo" />
                <ChatBot
                    title="My Bot"
                    theme={myTheme}
                    botName={"BookTrip_dev"}
                    welcomeMessage="Welcome, how can I help you?"
                    onComplete={this.handleComplete.bind(this)}
                    clearOnComplete={false}
                    conversationModeOn={false}
                    voiceEnabled={true}
                    voiceConfig={customVoiceConfig}
                />
            </div>
        </div>
    );
  }
}

export default HomePage;