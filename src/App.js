import React from 'react';

import HomePage from "./components/HomePage/HomePage";

import './App.scss';


export class App extends React.Component {
  constructor(props) {
    super(props);
  };
  
  render() {
    return (
      <div className="App">
        <div className="App-container">
            <HomePage />
        </div>
      </div>
    );
  }
}

export default App;
