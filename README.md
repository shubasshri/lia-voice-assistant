### LIA - Logitech Intelligent Assistant 
This is a simple web application that can answer user's queries via speech and text.
It's a virtual assistant that responds to the user's queries in the form of speech and text.

**Getting started**

1. Clone the repository via HTTPS or SSH into a desired directory.
2. Go to the cloned directory
3. Run **npm install** to install the dependencies
4. In the current directory, run **npm start**



